var x = prompt("Enter an integer value", "0");
var num = parseInt(x);

for (var i=1; i < num; i++) {
	if (i % 15 === 0) {
		console.log("FooBar");
	}
	else if(i % 5 === 0) {
		console.log("Bar");
	}
	else if(i % 3 === 0) {
		console.log("Foo");
	}
	else {
		console.log(i);
	}
}